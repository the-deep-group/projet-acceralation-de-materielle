import torch
import numpy as np
from torchvision import transforms
import os
from os import listdir
from PIL import Image
from torch import nn, optim
import torch.nn.functional as F
import matplotlib.pyplot as plt
import time

# initialisation des chemins d'accès
# PATH = "/home/thomas/Documents/Ecole/Semestre_09/Accel_matérielle/Projet/binary_dataset/"
PATH = "/bettik/PROJECTS/pr-material-acceleration/samanost/binary_dataset/binary_dataset/"
PATH_SEMANTIC_TRAIN = PATH + "images_semantic/train/"
PATH_ORIGINAL_TRAIN = PATH + "original_images/train/"
PATH_SEMANTIC_TEST = PATH + "images_semantic/test/"
PATH_ORIGINAL_TEST = PATH + "original_images/test/"
device = "cuda" if torch.cuda.is_available() else "cpu"

EPOCH = 10
BATCHSIZE = 5
lr = 0.001
width=512
height=512
N=100

def dir2input(directory,width,height,nbrchannel) :
    liste= []
    i=0
    for img in os.listdir(directory):
        image = Image.open(directory+img)
        image1 = transforms.ToTensor()(image)
        image1 = transforms.CenterCrop([width,height])(image1)
        liste.append(image1)
        i+=1
        if i >= N:
            break
    stack = torch.stack(liste, dim=0)
    return stack

def dirclass2input(directory,width,height) :
    liste= []
    i=0
    for img in os.listdir(directory):
        image = Image.open(directory+img)
        image = transforms.functional.rgb_to_grayscale(image)
        image1 = transforms.ToTensor()(image)
        image1 = torch.where(image1>0.55,1.,0.)
        image1 = transforms.CenterCrop([width,height])(image1)
        liste.append(image1)
        i+=1
        if i >= N:
            break
    stack = torch.stack(liste, dim=0)
    return stack


# initialisation des listes
semantics_train = dirclass2input(PATH_SEMANTIC_TRAIN, width, height)
originals_train = dir2input(PATH_ORIGINAL_TRAIN,width,height,3)
semantics_test = dirclass2input(PATH_SEMANTIC_TEST, width, height)
originals_test = dir2input(PATH_ORIGINAL_TEST,width,height,3)

print(semantics_train.shape)
print(semantics_test.shape)
print(originals_train.shape)
print(originals_test.shape)

def dice(input, target):
    smooth = 1.

    iflat = 1-input.view(-1)
    tflat = 1-target.view(-1)
    intersection = (iflat * tflat).sum()
    
    return ((2. * intersection + smooth) /
              (iflat.sum() + tflat.sum() + smooth))

N1 = 16
N2 = 16
MID = 16
CONV1 = 3
CONV2 = 3
CONV3 = 3
C1 = 2
C2 = 2
C3 = 2
NBCLASSE=1


class UNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3 , N1 , CONV1 , padding = (CONV1//2))       # width*height
        self.conv1b = nn.Conv2d(N1 , N1 , CONV1 , padding = (CONV1//2))     # width*height
        self.conv2 = nn.Conv2d(N1 , N2 , CONV2 , padding = (CONV2//2))      # (width-conv1+1)*(height-conv1+1)
        self.conv2b = nn.Conv2d(N2 , N2 , CONV2 , padding = (CONV2//2))

        self.conv3 = nn.Conv2d(N2 , MID , CONV3 , padding = (CONV3//2))
        self.conv3b = nn.Conv2d(MID , MID , CONV3 , padding = (CONV3//2))

        self.deconv1 = nn.ConvTranspose2d(MID , N2, CONV2, stride = (C2,C2), padding = (1,1))
        self.conv4 = nn.Conv2d(N2 + N2 , N2 , CONV3 , padding = (CONV3//2))
        self.conv4b = nn.Conv2d(N2 , N2 , CONV3 , padding = (CONV3//2))
        self.deconv2 = nn.ConvTranspose2d( N2, N1, CONV1, stride = (C1,C1), padding = (1,1))
        self.conv5 = nn.Conv2d(N1 + N1 , N1 , CONV3 , padding = (CONV3//2))
        self.conv5b = nn.Conv2d(N1 , N1 , CONV3 , padding = (CONV3//2))
        
        self.convFinal = nn.Conv2d( N1, NBCLASSE, 1)
    
    def forward(self, x):
                                                            # BATCHSIZE * 3 * height * width
        x1 = F.relu(self.conv1b(F.relu(self.conv1(x))))     # BATCHSIZE * 1ère ligne de reseau de neurone * height * width

        x = F.max_pool2d(x1,(C1,C1), padding = (1,1))       # BATCHSIZE * 1ère ligne de reseau de neurone * height/2 * width/2
        x2 = F.relu(self.conv2b(F.relu(self.conv2(x))))     # BATCHSIZE * 2ème ligne de reseau de neurone * height/2 * width/2
        x = F.max_pool2d(x2, (C2,C2), padding = (1,1))      # BATCHSIZE * 2ème ligne de reseau de neurone * height/4 * width/4
        x = F.relu(self.conv3b(F.relu(self.conv3(x))))      # BATCHSIZE * 3ème ligne de reseau de neurone * height/4 * width/4
        x = F.relu(self.deconv1(x))                         # BATCHSIZE * 3ème ligne de reseau de neurone * height/2 * width/2
        (_, _, H, W) = x.shape
        x2 = transforms.CenterCrop([H, W])(x2)
        x = torch.cat((x2,x),1)                             # BATCHSIZE * 3ème + 2ème ligne de reseau de neurone * height/2 * width/2
        x = F.relu(self.conv4b(F.relu(self.conv4(x))))      # BATCHSIZE * 4ème ligne de reseau de neurone * height/2 * width/2
        x = F.relu(self.deconv2(x))                         # BATCHSIZE * 4ème ligne de reseau de neurone * height * width
        (_, _, H, W) = x.shape
        x1 = transforms.CenterCrop([H, W])(x1) 
        x = torch.cat((x1,x),1)                             # BATCHSIZE * 4ème + 1ère ligne de reseau de neurone * height * width
        x = F.relu(self.conv5b(F.relu(self.conv5(x))))      # BATCHSIZE * 5ème ligne de reseau de neurone * height * width
        x = F.interpolate(x, size = (width , height))       # 20 * 5ème ligne de reseau de neurone * height * width
        x = self.convFinal(x)                               # 20 * 1 * height * width

        return x
    
model = UNet().to(device)
criterion = nn.BCEWithLogitsLoss(reduction = "mean")
# criterion = dice_loss
optimizer = optim.Adam(model.parameters(), lr=lr)
epochs = EPOCH
steps = 0
train_losses, test_losses = [], []
train_acc = []
test_acc = []
tepoque=0

for e in range(epochs):
    t = time.time()
    running_loss = 0
    i = 0
    accuracy_train = 0
    test_loss = 0
    test_accuracy = 0
    model.train()

    while i < len(originals_train):
        if i + BATCHSIZE < len(originals_train) :
            input , labels = (originals_train[ i : i + BATCHSIZE],semantics_train[ i : i + BATCHSIZE])
        else :
            input , labels = (originals_train[ i : len(originals_train) -1 ],semantics_train[ i : len(originals_train) -1 ])
        input = input.to(device)
        labels = labels.to(device)
        out = model( input )
        loss = criterion( out, labels )
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        output = torch.sigmoid( out )
        running_loss += loss.item()
        calcul = torch.relu( torch.sign( output - 0.5 ) )
        equals = ( calcul == labels ) # .reshape_as(calcul)
        accuracy_train += ( equals.long().sum().item() ) / ( width * height )
        # accuracy_train += dice(calcul,labels).item()
        i += BATCHSIZE

    # Turn off gradients for validation, saves memory and computations
    with torch.no_grad():
        model.eval()
        i = 0
        while i < len(originals_test):
            if i + BATCHSIZE < len(originals_test):
                input , labels = (originals_test[i:i+BATCHSIZE],semantics_test[i:i+BATCHSIZE])
            else :
                input , labels = (originals_test[i:len(originals_test)-1],semantics_test[i:len(originals_test)-1])
            input = input.to(device)
            labels = labels.to(device)
            out = model(input)
            loss = criterion(out, labels)
            test_loss += loss.item()

            output = torch.sigmoid(out)
            calcul = torch.relu( torch.sign( output - 0.5 ) )
            equals = ( calcul == labels ) # .reshape_as(calcul)
            test_accuracy += ( equals.long().sum().item() ) / ( width * height )
            # test_accuracy += dice(calcul,labels).item()
            i += BATCHSIZE

    train_losses.append( running_loss / ( len(semantics_train) / BATCHSIZE ) )      
    test_losses.append( test_loss / ( len(semantics_test) / BATCHSIZE ) )        
    train_acc.append(accuracy_train / len(semantics_train))
    test_acc.append(test_accuracy / len(semantics_test))
    g = time.time()
    tepoque = g-t
    if epochs < 10 or e % (epochs//10) == epochs//10 - 1 :
        print("Epoch: {}/{}.. ".format(e+1, epochs),
                "Training Loss: {:.3f}.. ".format(train_losses[-1]),
                "Training Accuracy: {:.3f}".format(accuracy_train/len(semantics_train)),
                "Test Loss: {:.3f}.. ".format(test_losses[-1]),
                "Test Accuracy: {:.3f}".format(test_accuracy/len(semantics_test)),
                "temps de l'époque (en min): = ",str(tepoque/60))
        tepoque = 0
    
LOSS = train_losses[-1]
torch.save({
            'epoch': EPOCH,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': LOSS,
            }, PATH + "Conv3.pt")
           
plt.figure()        
for i in range (6):
    out = model(originals_test[3*i : 3*i + 1])
    output = torch.sigmoid(out)
    output = torch.relu( torch.sign( output - 0.5 ) )
    estimation = output.detach().numpy() 
    reel = semantics_test[3*i : 3*i + 1].detach().numpy()
    plt.subplot(3,4,2*i+1); 
    plt.imshow(estimation.reshape((width,height)),label = "Exemple " + str(i) + " Epoch:" + str(EPOCH))
    plt.title('Prédiction')
    plt.subplot(3,4,2*i+2)
    plt.imshow(reel.reshape((width,height)), interpolation='none', label = "Exemple " + str(i) + " Epoch:" + str(EPOCH))
    plt.title('Réel')
plt.savefig(PATH + "Conv4.png",dpi=200,bbox_inches='tight')



# Change from list of tensors that lies on gpu to cpu
test_losses_cpu = [t for t in test_losses]
plt.figure()
plt.subplot(2,1,1)
plt.plot(train_losses, label='Training loss')
plt.plot(test_losses_cpu, label='Validation loss')
plt.subplot(2,1,2)
plt.plot(train_acc, label='Training accuracy')
plt.plot(test_acc, label='Validation accuracy')
plt.legend(frameon=False)
plt.savefig( PATH + "LearnCurve4",dpi=200,bbox_inches='tight')

reel = semantics_test[0]
