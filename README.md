# Projet Accéralation matérielle



## Getting started

The goal of this project is to perform binary segmentation of a dataset of images in order to detect pixels that belong to the floor from pixels that don't. The tests are done on a single GPU and the code's source is the following: https://pyimagesearch.com/2021/11/08/u-net-training-image-segmentation-models-in-pytorch/?fbclid=IwAR0FS96_oUXH73C_2toWrDK44jrV22gGA46HE6N1KPrOcda8Ep8AOkwWGzE

In this project, three architectures have been explored : the one available in the file UNET.py, and the 3 and 4-layer UNet architectures put respectively in the directories UNet3 and UNet4.

## Libraries

The following commands need to be done in the work envrironment before running the codes:

`pip3 install torch torchvision torchaudio`

`pip3 install matplotlib`

`pip3 install opencv-python`

`pip3 install imutils`

`pip3 install scikit-learn`

`pip3 install tqdm`

`pip3 install torchmetrics==0.11.4`

`pip3 install numpy`

`pip3 install pillow`

## UNET architectures

### UNET.py

This file provides a complete guide to one of the architecture used in the project as well as the training algorithm. In order to start training, run the code UNET.py. There is a variable PATH that needs to be changed according to the PATH to the train images and test images. The images are separated into 2 groups train and test. To use the code it needs a folder which separates the images into those 2 groups.
To use it 

### UNet3 and UNet4

#### Contents
The directories provide two different UNet architectures. Each directory contains the following files:
- config.py: configuration file for setting data and figures/output paths, as well as main parameters such as train/test split rate, learning rate, number of epochs, segmentation threshold and image size.
- dataset.py: file to apply transformations on image labels (masks binarisation) before storing the images and the corresponding labels.
- model.py : main program for network architecture
- train.py: main program for training, calculates, displays and plots the train and test loss, accuracy, IoU and Dice score. The plots are saved in the directory ../output

#### Executing the codes
To train the networks:
- Choose the model to train by choosing one of the two directories.
- Ajust the hyperparameters by editing the file config.py
- Run the code train.py

### Output
The directory contrains some plot results that were obtained during training.
