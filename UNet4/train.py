# USAGE
# python train.py
# import the necessary packages
import dataset
import model
import config
from dataset import SegmentationDataset
from model import UNet
from torch.nn import BCEWithLogitsLoss
from torch.optim import Adam
from torch.utils.data import DataLoader
from sklearn.model_selection import train_test_split
from torchvision import transforms
from imutils import paths
from tqdm import tqdm
import matplotlib.pyplot as plt
import torch
import time
import os
import ignite.metrics
import numpy as np
from torchmetrics.classification import BinaryJaccardIndex
from torchmetrics import Dice
from torchmetrics.classification import BinaryConfusionMatrix

metric = BinaryJaccardIndex().to(config.DEVICE)
dice = Dice(average='micro').to(config.DEVICE)
bcm = BinaryConfusionMatrix()

# load the image and mask filepaths in a sorted manner
imagePaths = sorted(list(paths.list_images(config.IMAGE_DATASET_PATH)))
maskPaths = sorted(list(paths.list_images(config.MASK_DATASET_PATH)))
# partition the data into training and testing splits using 80% of
# the data for training and the remaining 20% for testing
split = train_test_split(imagePaths, maskPaths,
	test_size=config.TEST_SPLIT, random_state=42)
# unpack the data split
(trainImages, testImages) = split[:2]
(trainMasks, testMasks) = split[2:]
# write the testing image paths to disk so that we can use then
# when evaluating/testing our model
print("[INFO] : Is CUDA enabled ?", torch.cuda.is_available())
print("[INFO] saving testing image paths...")
f = open(config.TEST_PATHS, "w")
f.write("\n".join(testImages))
f.close()

# define transformations
transforms = transforms.Compose([transforms.ToPILImage(),
 	transforms.Resize((config.INPUT_IMAGE_HEIGHT,
		config.INPUT_IMAGE_WIDTH)),
	transforms.ToTensor()])
# create the train and test datasets
trainDS = SegmentationDataset(imagePaths=trainImages, maskPaths=trainMasks,
	transforms=transforms)
testDS = SegmentationDataset(imagePaths=testImages, maskPaths=testMasks,
    transforms=transforms)
print(f"[INFO] found {len(trainDS)} examples in the training set...")
print(f"[INFO] found {len(testDS)} examples in the test set...")
# create the training and test data loaders
trainLoader = DataLoader(trainDS, shuffle=True,
	batch_size=config.BATCH_SIZE, pin_memory=config.PIN_MEMORY)
testLoader = DataLoader(testDS, shuffle=False,
	batch_size=config.BATCH_SIZE, pin_memory=config.PIN_MEMORY)

# initialize our UNet model
unet = UNet().to(config.DEVICE)
# initialize loss function and optimizer
lossFunc = BCEWithLogitsLoss()
opt = Adam(unet.parameters(), lr=config.INIT_LR)
# calculate steps per epoch for training and test set
trainSteps = len(trainDS) // config.BATCH_SIZE
testSteps = len(testDS) // config.BATCH_SIZE
# initialize a dictionary to store training history
H = {"train_loss": [], "test_loss": [], "train_acc": [], "test_acc": [], "train_IoU": [], "test_IoU": [], "train_dice": [], "test_dice": []}

# loop over epochs
print("[INFO] training the network...")
startTime = time.time()
for e in tqdm(range(config.NUM_EPOCHS)):
	# set the model in training mode
	unet.train()
	# initialize the total training and validation loss
	totalTrainLoss = 0
	totalTestLoss = 0
	totalTrainAcc = 0
	totalTestAcc = 0
	totalTrainIoU = 0
	totalTestIoU = 0
	totalTrainDice = 0
	totalTestDice = 0
    # loop over the training set
	for (i, (x, y)) in enumerate(trainLoader):
		# send the input to the device
		(x, y) = (x.to(config.DEVICE), y.to(config.DEVICE))
		# perform a forward pass and calculate the training loss
		acc = 0
		pred = unet(x)
		loss = lossFunc(pred, y)
		prediction = torch.sigmoid(pred)
		prediction[prediction >= 0.5] = 1
		prediction[prediction < 0.5] = 0
		acc = (y == prediction).sum()
		acc = acc/(config.BATCH_SIZE*config.INPUT_IMAGE_HEIGHT*config.INPUT_IMAGE_WIDTH)
		train_IoU = metric(prediction[0,0,:,:], y[0,0,:,:])
		train_dice = dice(prediction[0,0,:,:], y[0,0,:,:].type(torch.int64))
        # first, zero out any previously accumulated gradients, then
		# perform backpropagation, and then update model parameters
		opt.zero_grad()
		loss.backward()
		opt.step()
		# add the loss to the total training loss so far
		totalTrainLoss += loss
		totalTrainAcc += acc
		totalTrainIoU += train_IoU
		totalTrainDice += train_dice
	# switch off autograd
	with torch.no_grad():
		# set the model in evaluation mode
		unet.eval()
		# loop over the validation set
		for (x, y) in testLoader:
			# send the input to the device
			(x, y) = (x.to(config.DEVICE), y.to(config.DEVICE))
			# make the predictions and calculate the validation loss
			accuracy = 0
			pred = unet(x)
			test_predic = torch.sigmoid(pred)
			test_predic[test_predic >= 0.5] = 1
			test_predic[test_predic < 0.5] = 0
			accuracy = (y == test_predic).sum()
			accuracy = accuracy / (config.BATCH_SIZE*config.INPUT_IMAGE_HEIGHT*config.INPUT_IMAGE_WIDTH)
			test_IoU = metric(test_predic[0,0,:,:], y[0,0,:,:])
			test_dice = dice(test_predic[0,0,:,:], y[0,0,:,:].type(torch.int64))
			totalTestLoss += lossFunc(pred, y)
			totalTestAcc += accuracy
			totalTestIoU += test_IoU
			totalTestDice += test_dice
	# calculate the average training and validation loss
	avgTrainLoss = totalTrainLoss / trainSteps
	avgTestLoss = totalTestLoss / testSteps
	avgTrainAcc = totalTrainAcc / trainSteps
	avgTestAcc = totalTestAcc / testSteps
	avgTrainIoU = totalTrainIoU / trainSteps
	avgTestIoU = totalTestIoU / testSteps
	avgTrainDice = totalTrainDice / trainSteps
	avgTestDice = totalTestDice /testSteps
	# update our training history
	H["train_loss"].append(avgTrainLoss)
	H["test_loss"].append(avgTestLoss)
	H["train_acc"].append(avgTrainAcc)
	H["test_acc"].append(avgTestAcc)
	H["train_IoU"].append(avgTrainIoU)
	H["test_IoU"].append(avgTestIoU)
	H["train_dice"].append(avgTrainDice)
	H["test_dice"].append(avgTestDice)
	# print the model training and validation information
	print("[INFO] EPOCH: {}/{}".format(e + 1, config.NUM_EPOCHS))
	print("Train loss: {:.6f}, Test loss: {:.6f}, Train acc: {:.6f}, Test acc: {:.6f},  Train IoU: {:.6f}, Test IoU: {:.6f},  Train Dice: {:.6f}, Test Dice: {:.6f}".format(
		avgTrainLoss, avgTestLoss, avgTrainAcc, avgTestAcc, avgTrainIoU, avgTestIoU, avgTrainDice, avgTestDice))
# display the total time needed to perform the training
endTime = time.time()
print("[INFO] total time taken to train the model: {:.2f}s".format(
	endTime - startTime))


# Plotting one of the images
image = testDS[5][0]
reel = testDS[5][1].to(config.DEVICE)
image = np.expand_dims(image, 0)
image = torch.from_numpy(image).to(config.DEVICE)
estimation = unet(image)
estimation = torch.sigmoid(estimation)
estimation[estimation >= 0.5] = 1
estimation[estimation < 0.5] = 0
print("IoU: {:.6f}".format(metric(estimation[0,0,:,:], reel[0,:,:])))
print("Dice: {:.6f}".format(dice(estimation[0,0,:,:], reel[0,:,:].type(torch.int64))))


# Plotting the loss, accuracy and IoU score
plt.figure()
H["train_loss"] = [(t.cpu()).detach().numpy() for t in H["train_loss"]]
H["test_loss"] = [(t.cpu()).detach().numpy() for t in H["test_loss"]]
H["train_acc"] = [(t.cpu()).detach().numpy() for t in H["train_acc"]]
H["test_acc"] = [(t.cpu()).detach().numpy() for t in H["test_acc"]]
H["train_IoU"] = [(t.cpu()).detach().numpy() for t in H["train_IoU"]]
H["test_IoU"] = [(t.cpu()).detach().numpy() for t in H["test_IoU"]]
H["train_dice"] = [(t.cpu()).detach().numpy() for t in H["train_dice"]]
H["test_dice"] = [(t.cpu()).detach().numpy() for t in H["test_dice"]]
plt.plot(H["train_loss"], label="train_loss")
plt.plot(H["test_loss"], label="test_loss")
plt.plot(H["train_acc"], label="train_accuracy")
plt.plot(H["test_acc"], label="test_accuracy")
plt.title("Metrics of the training")
plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.legend(loc = "lower left")
plt.savefig(config.PLOT_PATH)
plt.figure()
plt.plot(H["train_IoU"], label="train_IoU")
plt.plot(H["test_IoU"], label="test_IoU")
plt.plot(H["train_dice"], label="train_dice")
plt.plot(H["test_dice"], label="test_dice")
plt.title("Metrics of the training")
plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.legend(loc="lower left")
plt.savefig(config.PLOT_PATH2)
