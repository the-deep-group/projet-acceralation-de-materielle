from torch.utils.data import Dataset
import cv2

class SegmentationDataset(Dataset):
    def __init__(self, imagePaths, maskPaths, transforms):
        # Store the image and mask filepaths, and augmentation transforms
        self.imagePaths = imagePaths
        self.maskPaths = maskPaths
        self.transforms = transforms
        
    def __len__(self):
        # Return the number of total samples contained in the dataset
        return len(self.imagePaths)
    
    def __getitem__(self, idx):
		# Grab the image path from the current index
        imagePath = self.imagePaths[idx]
        
        # Load image and masks
        image = cv2.imread(imagePath)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        mask = cv2.imread(self.maskPaths[idx], 0)
        
        # Check to see if we are applying any transformations
        if self.transforms is not None : 
            # Apply the transformation to both the image and its mask
            image = self.transforms(image)
            mask = self.transforms(mask)
            mask[mask > 0.5] = 1
            mask[mask <= 0.5] = 0
            
        return (image, mask)