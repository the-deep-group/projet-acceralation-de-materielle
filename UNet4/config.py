# Importing the necessary packages
import os
import torch

# Base path of the dataset
DATASET_PATH = "/bettik/PROJECTS/pr-material-acceleration/COMMON/binary_dataset/binary_dataset/"

# Path to images and masks dataset
IMAGE_DATASET_PATH = os.path.join(DATASET_PATH, "original_images")
MASK_DATASET_PATH = os.path.join(DATASET_PATH, "images_semantic")

# Defining the test split
TEST_SPLIT = 0.125

# Device to be used for training and evaluation
DEVICE = "cuda" if torch.cuda.is_available() else "cpu"

# Determining whether memory will be pinned during data loading
PIN_MEMORY = True if DEVICE == "cuda" else False

# Number of channels in the input, number of classes and number of levels in the U-Net model
NUM_CHANNELS = 1
NUM_CLASSES = 1
NUM_LEVELS = 4

# Learning rate, number of epochs and Batch size
INIT_LR = 0.001
NUM_EPOCHS = 200
BATCH_SIZE = 10

# Input image dimensions
INPUT_IMAGE_WIDTH = 450
INPUT_IMAGE_HEIGHT = 368

# Defining threshold to filter weak predicitions
THRESHOLD = 0.5

# Base directory for the output
BASE_OUTPUT = "../output"

# define the path to the output serialized model, model training
# plot, and testing image paths
MODEL_PATH = os.path.join(BASE_OUTPUT, "unet4.pth")
PLOT_PATH = os.path.sep.join([BASE_OUTPUT, "plot_loss_acc_unet4.png"])
PLOT_PATH2 = os.path.sep.join([BASE_OUTPUT, "plot_IoU_dice_unet4.png"])
TEST_PATHS = os.path.sep.join([BASE_OUTPUT, "test_paths.txt"])

